#include "Set.h"
#include "SetITerator.h"

#include<iostream>
using namespace std;

Set::Set() {
	//TODO - Implementation
	this->capacity = 10;		// initialize capacity with 10
	this->actualSize = 0;		// initialize actualSize with 0 since we have no elements
	this->head = -1;			// initialize the head with -1 since we don't have one
	this->firstEmpty = 0;		// initialize the first empty as 0 meaning the start of the array
	this->array.elements = new TElem[10];	// create new array for values
	this->array.next = new int[10];			// create new array for next ints
	for (int i = 0; i < 10; i++){			// set each next int as the next index
		this->array.next[i] = i + 1;
	}
	this->array.next[9] = -1;				// set the last one as -1 since this is no circular array
}
//Theta(n = 10 in this case) since we need to initiate all the nexts


bool Set::add(TElem elem) {
	//TODO - Implementation
	if (this->firstEmpty == -1) {	// The array is full
		TElem* newElems;	// create new array for values
		int* newNext;		// create new array for next
		newElems = new TElem[this->capacity * 2];	// double the size of the previous one
		newNext = new int[this->capacity * 2];		// double the size of the previous one
		for (int i = 0; i < this->capacity; i++) {	// Iterate through the new arrays
			newElems[i] = this->array.elements[i];		// Store the old values into the new one
			newNext[i] = this->array.next[i];
		}
		for (int i = this->capacity; i < this->capacity * 2; i++) {
			newNext[i] = i + 1;		// Create the new next's for the new part of the array
		}
		newNext[this->capacity * 2 - 1] = -1;	// Mark the last next as -1 so that we know it as the last

		delete[] this->array.elements;	// Free the memory
		delete[] this->array.next;	// Free the memory

		this->array.elements = newElems;	// Set it to point to the new one
		this->array.next = newNext;	// Set it to point to the new one
		this->firstEmpty = this->capacity + 1;	// The first empty is the first space from the second part of the new array
		this->capacity = this->capacity * 2;	// The capacity doubled !! WOW !!
	}
	if (this->head == -1) // we do not have a head
	{
		this->head = firstEmpty;						// save the index for the head
		this->actualSize++;								// increase the actual size
		this->array.elements[this->head] = elem;		// save the value
		this->firstEmpty = this->array.next[this->head];// store the new firstEmpty
		this->array.next[this->head] = -1;				// the next will be a 'nullptr' basically
		return true;
	}
	else {
		int currentNode = this->head;			// Start from the head
		int prevNode = -1;						// Create a previous node
		while (currentNode != -1 && this->array.elements[currentNode] != elem) {	// Iterate until the end or until we find an element with the same value
			prevNode = currentNode;						// prevNode becomes the current node
			currentNode = this->array.next[currentNode];// The current node increases, thus we saved the last node and the actual node
		}
		if (currentNode == -1) {	// we reached the end, this means we did not find a duplicate
			this->array.next[prevNode] = this->firstEmpty;					// make the last element in the list point to the firstEmpty slot
			this->array.elements[this->array.next[prevNode]] = elem;		// set the value for the new element
			this->firstEmpty = this->array.next[this->array.next[prevNode]];// set the new firstEmpty
			this->array.next[this->array.next[prevNode]] = -1;				// Since this is the last element, its next will be -1
			this->actualSize++;												// increase the actual size
			return true;
		}
		else return false;
	}
	return false;
}
// Best case would be Omega(1) if we can add to the head, but Average Case = Worst Case = Theta(n) since most of the times we iterate
// till we reach the end or an element with the same value thus returning false
// There is also a case when we need to increase size which would technically add a Theta(n) to the complexity, but since it happends
// every so often, it could be amortized as Theta(1)


bool Set::remove(TElem elem) {
	//TODO - Implementation
	if (this->actualSize < this->capacity / 2 - 1 && this->actualSize > 1000) {	// if our array is too big
		// resize
		TElem* newElems;	// create new array for values
		int* newNext;		// create new array for next
		this->capacity = this->capacity / 2;	// The capacity halfened !! WOW !!
		newElems = new TElem[this->capacity];	// halfen the size of the previous one
		newNext = new int[this->capacity];		// halfen the size of the previous one
		int counter = 0;						// we are going to add the elements in the new array in cronological order now (from 0 to max)
		int currentNode = this->head;
		while (this->array.next[currentNode] != -1) {				// Iterate through all elements
			newElems[counter] = this->array.elements[currentNode];	// Save on counter position the currentNode elem
			newNext[counter] = this->array.next[currentNode];		// Save on counter position the currentNode next
			currentNode = this->array.next[currentNode];			// Increate currentNode
		}
		newNext[counter++] = -1;									// Set the next fot this->array.next[firstEmpty] as -1
		delete[] this->array.elements;
		delete[] this->array.next;

		this->head = 0;						// Set the heas as 0 because we reimagined the array
		this->array.elements = newElems;	// Set it to point to the new one
		this->array.next = newNext;			// Set it to point to the new one
		this->firstEmpty = counter;			// The first empty becomes the last element we added to
	}

	if (this->array.elements[this->head] == elem) {		// Check if the head is the element we are looking for (since this is a special case)
		int newPos = this->array.next[this->head];		// Save the current next of the head since we will lose the head.next soon
		this->array.next[this->head] = this->firstEmpty;// Save the first empty we have as the next of the old head (so we can use it when we add a new element)
		this->firstEmpty = this->head;					// Make the first empty point to the position of the old head
		this->head = newPos;							// Make the head point to the heads old next (newPos, meaning the second element from the list)
		this->actualSize--;								// Decrease the actual size
		return true;
	}
	// if we got here it means the head was not what we were looking for
	int currentNode = this->head;												// Start from the head
	int prevNode = -1;															// Save a previous node
	while (currentNode != -1 && this->array.elements[currentNode] != elem) {	// Iterate until we either reach the end or reach the needed element
		prevNode = currentNode;							// Previous node becomes current node
		currentNode = this->array.next[currentNode];	// Current node goes on to the next one, thus we store current and last
	}
	if (currentNode != -1) {	// meaning we did not reach the end, thus we found the element
		{
			this->array.next[prevNode] = this->array.next[currentNode];			// Remove the inbetween pointer by making the last point to the next
			this->array.next[currentNode] = this->firstEmpty;					// Make the pointer of current node point to the last firstEmpty so we can reuse it later
			this->firstEmpty = currentNode;										// Make the first empty be the one we removed
			this->actualSize--;													// Decrease actual size
			return true;
		}
	}
	return false;
}
// Best case would be Omega(1) if we can remove the the head, but Average Case = Worst Case = Theta(n) since most of the times we iterate
// till we reach the end or an element with the required value, thus returning true
// There is also a case when we need to decrease size which would technically add a Theta(n) to the complexity, but since it happends
// every so often, it could be amortized as Theta(1)

bool Set::search(TElem elem) const {
	//TODO - Implementationint currentNode = this->head;
	int currentNode = this->head;		// Start from the head
	while (currentNode != -1 && this->array.elements[currentNode] != elem) {	// Iterate through the list until the end or until we find the needed element
		currentNode = this->array.next[currentNode];
	}
	if (currentNode == -1) {	// we reached the end, this means we did not find the element
		return false;
	}
	else return true;
}
// Best case: Omega(1) when the head is the needed element, we don't exactly iterate, we just start and end immediatelly
// Average Case = Worst case = Theta(n) when we iterate through the list and find the element somewhere in there


int Set::size() const {
	//TODO - Implementation
	return this->actualSize;
}
// Theta(1)


bool Set::isEmpty() const {
	//TODO - Implementation
	if (this->size() == 0)	return true;
	else return false;
}
// Theta(1)


Set::~Set() {
	//TODO - Implementation
	if (!this->isEmpty()) {
		delete[] this->array.elements;
		delete[] this->array.next;
		this->actualSize = 0;
		this->capacity = 0;
		this->firstEmpty = -1;
		this->head = -1;
	}
}
// Looking at what we wrote it would technically be Theta(1), but since every delete[] is a Theta(n), we will have:
// Best Case: Omega(1), Worst Case = Average Case = Theta(n) (because we delete arrays)

SetIterator Set::iterator() const {
	return SetIterator(*this);
}
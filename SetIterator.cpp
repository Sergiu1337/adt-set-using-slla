#include "SetIterator.h"
#include "Set.h"

#include<exception>
using namespace std;

SetIterator::SetIterator(const Set& m) : set(m)
{
	//TODO - Implementation
	this->currentNode = m.head;
	this->setSize = 0;
}
// Theta(1)

void SetIterator::first() {
	//TODO - Implementation
	this->currentNode = set.head;
	this->setSize = 0;
}
// Theta(1)


void SetIterator::next() {
	//TODO - Implementation
	if (this->valid()) {
		this->currentNode = this->set.array.next[this->currentNode];
		this->setSize++;
	}
	else throw exception();
}
// Theta(1)


TElem SetIterator::getCurrent()
{
	//TODO - Implementation
	if (this->currentNode == -1)
		throw exception();
	return set.array.elements[this->currentNode];
}
// Theta(1)

bool SetIterator::valid() const {
	//TODO - Implementation
	if (this->set.isEmpty())
		return false;
	else if (this->setSize < this->set.size())
		return true;
	else
		return false;
}
// Theta(1)